user = User.create email: 'rvsantos@email.com', password: '123456', password_confirmation: '123456'

Category.create [
  { name: 'Programming' },
  { name: 'Event' },
  { name: 'Travel' },
  { name: 'Music' },
  { name: 'TV' }
]

user.articles.create([
                       {
                         title: 'Advanced Active Record',
                         body: 'Models need to relate to each other. In the real world, ..',
                         published_at: Date.today
                       },
                       {
                         title: 'One-to-many associations',
                         body: 'One-to-many associations describe a pattern ..',
                         published_at: Date.today
                       },
                       {
                         title: 'Associations',
                         body: 'Active Record makes working with associations easy..',
                         published_at: Date.today
                       }
                     ])
