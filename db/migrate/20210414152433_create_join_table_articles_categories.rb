class CreateJoinTableArticlesCategories < ActiveRecord::Migration[6.1]
  def change
    create_join_table :articles, :categories do |t|
      t.index %i[article_id category_id]
      t.index %i[category_id article_id]
    end
  end
end
